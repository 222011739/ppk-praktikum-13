> Nama : Muhammad Naufal Faishal            
Kelas / Absen : 3SI3 / 27

## Laporan Praktikum 13

1. Kegiatan Praktikum (Menggunakan AsyncTask)
        >![AsyncTask1](/images/AsyncTask1.png "AsyncTask1")
        >![AsyncTask2](/images/AsyncTask2.png "AsyncTask2")

1. Penugasan (Menggunakan java.util.concurrent)
        >![Concurrent1](/images/Concurrent1.png "Concurrent1")
        >![Concurrent2](/images/Concurrent2.png "Concurrent2")
        