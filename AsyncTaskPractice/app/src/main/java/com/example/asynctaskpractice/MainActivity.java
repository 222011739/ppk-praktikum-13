package com.example.asynctaskpractice;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {
    URL ImageUrl = null;
    InputStream is = null;
    Bitmap bmImg = null;
    ImageView imageView = null;
    Button button = null;
    ProgressDialog p;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = findViewById(R.id.asyncTask);
        imageView = findViewById(R.id.image);
        button.setOnClickListener(view -> {
            ExecutorService service = Executors.newSingleThreadExecutor();
            service.execute(() -> {
                runOnUiThread(() -> {
                    p = new ProgressDialog(MainActivity.this);
                    p.setMessage("Downloading...");
                    p.setIndeterminate(false);
                    p.setCancelable(false);
                    p.show();
                });

                try {
                    ImageUrl = new URL("https://i.ibb.co/WcjR2cR/Logo-Buat-Sikoko.png");
                    HttpURLConnection httpURLConnection = (HttpURLConnection) ImageUrl.openConnection();
                    httpURLConnection.connect();
                    bmImg = BitmapFactory.decodeStream(httpURLConnection.getInputStream());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                runOnUiThread(() -> {
                    p.dismiss();
                    imageView.setImageBitmap(bmImg);
                });
            });
        });

    }
}